package e.n.amoledify;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    ImageView imageViewLogo,imageViewText;
    Animation animationSlideUp;
    Animation animationFadeIn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageViewLogo = findViewById(R.id.imageViewLogo);
        imageViewText= findViewById(R.id.imageViewText);

        animationSlideUp = AnimationUtils.loadAnimation(this, R.anim.slide_up_anim);
        imageViewLogo.startAnimation(animationSlideUp);


        animationFadeIn=AnimationUtils.loadAnimation(this,R.anim.fade_anim);
        imageViewText.startAnimation(animationFadeIn);
    }
}
